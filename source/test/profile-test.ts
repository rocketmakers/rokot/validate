import { expect, sinon, supertest } from "rokot-test";
import { IValidator, Validator, IValidatorSpec, validate, ConstraintSpec, IValidationError } from "../index";
import { IProfile, ProfileValidator } from "./example/profile";

const p1: IProfile = { id: 1, isActive: true, roleIds: [1, 2], friends: [] };
const p2: IProfile = { id: 2, isActive: true, roleIds: [1, 2], friends: [] };
const p3: IProfile = { id: 3, isActive: true, roleIds: [1, 2], friends: [] };
const p4: IProfile = { id: 4, isActive: true, roleIds: [1, 2], friends: [] };
const p5: IProfile = { id: 5, isActive: true, roleIds: [1, 2], friends: [] };

relate(p1, p2);
relate(p1, p3);
relate(p1, p4);
relate(p4, p5);

function relate(profile1: IProfile, profile2: IProfile) {
  if (profile1.id === profile2.id) {
    return;
  }
  profile1.friends.push(profile2)
  profile2.friends.push(profile1)
}

function invalidProfile(): IProfile {
  return { id: "abc" as any, isActive: null, roleIds: [0, 2], friends: [p1, { isActive: "yo" } as any as IProfile] }
}

function expectInvalidProfile(err) {
  const errors: IValidationError[] = err.errors;
  expect(errors).to.exist;
  expect(errors.length).to.be.eq(5);
  expect(errors[0].attribute).to.be.eq("id");
  expect(errors[1].attribute).to.be.eq("isActive");
  expect(errors[2].attribute).to.be.eq("roleIds");
  expect(errors[3].attribute).to.be.eq("friends.1.id");
  expect(errors[4].attribute).to.be.eq("friends.1.isActive");
}

describe("Profile Validator", () => {
  it("should fail on invalid Profile", () => {
    return expect(new ProfileValidator().validate(invalidProfile())).to.eventually.be.rejected.then(err => {
      expectInvalidProfile(err)
    });
  });

  it("should fail on invalid Profile with a catch", () => {
    return new ProfileValidator().validate(invalidProfile()).catch(err => {
      expectInvalidProfile(err)
    });
  });

  it("should validate with valid Profile", () => {
    return expect(new ProfileValidator().validate(p1)).to.eventually.be.fulfilled.deep.equal(p1);
  });

  it("should validate with valid Profile and strip extra properties", () => {
    var clone = validate.extend<IProfile>({ stripMe: true } as any, p1);
    return expect(new ProfileValidator().validate(clone)).to.eventually.be.fulfilled.deep.equal(p1).then(v => {
      expect(v.stripMe).to.be.undefined;
    });
  });
})
