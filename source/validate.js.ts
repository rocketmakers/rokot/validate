import { IValidationErrors } from "./errors";

// Custom Validator

export interface IValidator<T> {
  validate(instance: any): Promise<T>;
}

export interface IValidatorsSpec {
  [key: string]: IValidatorSpec<any>;
}

export interface IValidatorSpecWithContext<T, TContext> extends IValidatorSpec<T> {
  getContext(instance: any, isRootValidator?: boolean): Promise<TContext>;
}

export interface IValidatorSpec<T> {
  constraints(context: any): ConstraintSpec<T>;
}

// Custom
export interface ClientConstraints {
  string?: Constraint | boolean;
  boolean?: Constraint | boolean;
  array?: ArrayConstraint | boolean;
  absence?: Constraint | boolean;
  defined?: Constraint | boolean;
  spec?: ClientConstraintSpec<any>;
}

export interface Constraints {
  string?: ConstraintOrFactory<Constraint> | boolean;
  boolean?: ConstraintOrFactory<Constraint> | boolean;
  array?: ConstraintOrFactory<ArrayConstraint> | boolean;
  absence?: ConstraintOrFactory<Constraint> | boolean;
  defined?: ConstraintOrFactory<Constraint> | boolean;
  must?: MustConstraint<any> | MustConstraint<any>[];
  validator?: IValidatorSpec<any>;
  spec?: ConstraintSpec<any>
}

export type MustConstraintFunctionReturnType = Promise<boolean | string | string[]> | boolean | string | string[]

export interface MustConstraintFunction<T> {
  (value: T | T[], attributes: any): MustConstraintFunctionReturnType;
}

export interface ArrayConstraint extends Constraint {
  numericality?: NumericalityConstraint;
  string?: Constraint | boolean;
}

export interface MustConstraint<T> extends Constraint {
  func: MustConstraintFunction<T>;
}

// Library
export type ClientConstraintSpec<T> = Record<keyof T, ClientConstraints>

export interface ClientConstraints {
  date?: DateConstraint | boolean;
  datetime?: DateTimeConstraint | boolean;
  email?: Constraint | boolean;
  equality?: EqualityConstraint | string
  exclusion?: InclusionExclusionConstraint | any[];
  format?: FormatConstraint | string;
  inclusion?: InclusionExclusionConstraint | any[];
  length?: LengthConstraint;
  numericality?: NumericalityConstraint | boolean;
  presence?: Constraint | boolean;
  url?: UrlConstraint | boolean;
}

export type ConstraintSpec<T> = Record<keyof T, Constraints>

export interface Constraints {
  date?: ConstraintOrFactory<DateConstraint> | boolean;
  datetime?: ConstraintOrFactory<DateTimeConstraint> | boolean;
  email?: ConstraintOrFactory<Constraint> | boolean;
  equality?: ConstraintOrFactory<EqualityConstraint> | string
  exclusion?: ConstraintOrFactory<InclusionExclusionConstraint> | any[];
  format?: ConstraintOrFactory<FormatConstraint> | string;
  inclusion?: ConstraintOrFactory<InclusionExclusionConstraint> | any[];
  length?: ConstraintOrFactory<LengthConstraint>;
  numericality?: ConstraintOrFactory<NumericalityConstraint> | boolean;
  presence?: ConstraintOrFactory<Constraint> | boolean;
  url?: ConstraintOrFactory<UrlConstraint> | boolean;
}

export interface ConstraintFactory<T extends Constraint> {
  (value: any, attributes: any, attributeName: string, options: any, constraints: any): T;
}

export type ConstraintOrFactory<T extends Constraint> = T | ConstraintFactory<T>;

export interface Constraint {
  message?: string;
}

export interface DateTimeConstraint extends Constraint {
  earliest?: any;
  latest?: any;
  dateOnly?: boolean;
}

export interface DateConstraint extends Constraint {
  earliest?: any;
  latest?: any;
}

export interface EqualityConstraint extends Constraint {
  attribute: string;
  comparator?: (v1: any, v2: any) => boolean;
}

export interface InclusionExclusionConstraint extends Constraint {
  within: any[] | any;
}

export interface FormatConstraint extends Constraint {
  pattern: string | RegExp;
  flags?: string;
}

export interface LengthConstraint extends Constraint {
  is?: number;
  minimum?: number;
  maximum?: number;
  tokenizer?: (v: any) => TokenizerResult;
}

export interface NumericalityConstraint extends Constraint {
  onlyInteger?: boolean;
  noStrings?: boolean;
  greaterThan?: number;
  greaterThanOrEqualTo?: number;
  equalTo?: number;
  lessThanOrEqualTo?: number;
  lessThan?: number;
  odd?: boolean;
  even?: boolean;
}

export interface UrlConstraint extends Constraint {
  schemes?: string[];
  allowLocal?: boolean;
}

export interface TokenizerResult {
  length: number;
}

export interface ValidateJsValidator {
  <T>(attributes: any, constraints: ConstraintSpec<T>, options?: any): any[];
  async<T>(attributes: any, constraints: ConstraintSpec<T>, options?: any): Promise<any[]>;
  single<T>(value: any, constraints: ConstraintSpec<T>, options?: any): any[];
  cleanAttributes<T>(attributes: any, constraints: ConstraintSpec<any> | { [key: string]: boolean }): T;
  validators: any;
  extend<T>(validator: T, ...extensions: T[]): T;
  Promise: any;

  capitalize(value: string): string;
  collectFormValues(rootElement, [options]): any;
  contains(collection: any[] | any, value: any): boolean;
  getDeepObjectValue(object: any, keypath: string): any

  isInteger(value: any): value is number;
  isString(value: any): value is string;
  isArray(value: any): value is any[];
  isDefined(value: any): boolean;
  isFunction(value: any): boolean;
  isDate(value: any): boolean;
  isDomElement(value: any): boolean;
  isEmpty(value: any): boolean;
  isNumber(value: any): value is number;
  isObject(value: any): boolean;
  isPromise(value: any): boolean;

  format(formatString: string, values: any): string;
}
