import { expect, sinon, supertest, mocha } from "rokot-test";
import { IValidator, Validator, IValidatorSpec, validate, ConstraintSpec, IValidationError } from "../index";
import { IInvalid, InvalidValidator } from "./example/invalid";
import "chai-as-promised";
function validEntity(): IInvalid {
  return { id: 1, roleIds: [1, 2], isActive: true, errorEarly: "boom" }
}

describe("Invalid Case Validator", () => {
  it("should fail with RuntimeError", () => {
    var entity = validEntity();
    return expect(new InvalidValidator().validate(entity)).to.eventually.be.rejected.then(err => {
      const errors: Error = err;
      expect(errors).to.exist;
      expect(errors.message).to.be.eq("RuntimeError");
    });
  });
})
