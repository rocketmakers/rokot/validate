export declare type IError = Error;

export interface IValidationError {
  message: string;
  attribute: string;
  value: any;
}

export interface IValidationErrors {
  errors: IValidationError[];
}

export module Errors {
  export declare class Error implements IError {
    name: string;
    message: string;
    stack: string;
    static captureStackTrace(object, objectConstructor?);
  }

  export enum ErrorType {
    ValidationError = 1,
    NoInputError = 2
  }

  export abstract class CustomError extends Error {
    constructor(type: ErrorType, message: string, public internalMessage?: string) {
      super();
      this.name = ErrorType[type];
      this.message = message;
    }
  }

  export class ValidationError extends CustomError implements IValidationErrors {
    private ___typename___ = "Errors.ValidationError"
    constructor(public errors: IValidationError[]) {
      super(ErrorType.ValidationError, "Validation error");
      Error.captureStackTrace(this, this.constructor);
    }
  }

  export class NoValidationInputError extends CustomError {
    private ___typename___ = "Errors.NoValidationInputError"
    constructor() {
      super(ErrorType.NoInputError, "No input provided");
      Error.captureStackTrace(this, this.constructor);
    }
  }
}

export function isValidationError(error): error is Errors.ValidationError {
  return error["___typename___"] === "Errors.ValidationError"
}

export function isNoValidationInputError(error): error is Errors.NoValidationInputError {
  return error["___typename___"] === "Errors.NoValidationInputError"
}
