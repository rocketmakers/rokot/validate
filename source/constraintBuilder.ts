import { validate, isPromise } from "./validateJsWithCustomValidators";
import { NumericalityConstraint, Constraints, ClientConstraintSpec, ClientConstraints, Constraint, ArrayConstraint, ConstraintOrFactory, MustConstraintFunctionReturnType, MustConstraintFunction, IValidatorSpec } from "./validate.js";

export class ClientConstraintBuilder {
  /** array = (param) or true */
  public array<T>(arrayConstraint?: ArrayConstraint, extend?: ClientConstraints): ClientConstraints {
    return validate.extend({
      array: arrayConstraint || true
    }, extend)
  }

  /** array = (param) or true, presence = true */
  public arrayMandatory<T>(arrayConstraint?: ArrayConstraint, extend?: ClientConstraints): ClientConstraints {
    return validate.extend({
      array: arrayConstraint || true,
      presence: true
    }, extend)
  }

  /** array = true, validator (param) */
  public arrayValidator<T>(spec: ClientConstraintSpec<T>, extend?: ClientConstraints): ClientConstraints {
    return validate.extend({
      array: true,
      spec: spec
    }, extend)
  }

  /** array = true, presence = true, validator (param) */
  public arrayValidatorMandatory<T>(spec: ClientConstraintSpec<T>, extend?: ClientConstraints): ClientConstraints {
    return validate.extend({
      array: true,
      presence: true,
      spec: spec
    }, extend)
  }
  
  /** spec (param) */
  public itemValidator<T>(spec: ClientConstraintSpec<T>, extend?: ClientConstraints): ClientConstraints {
    return validate.extend({
      spec: spec
    }, extend)
  }

  /**  presence = true, spec (param) */
  public itemValidatorMandatory<T>(spec: ClientConstraintSpec<T>, extend?: ClientConstraints): ClientConstraints {
    return validate.extend({
      presence: true,
      spec: spec
    }, extend)
  }

  /** onlyInteger = true, noStrings = true */
  public integerStrict(numericality?: NumericalityConstraint, extend?: ClientConstraints): ClientConstraints {
    return validate.extend({
      numericality: validate.extend({
        onlyInteger: true,
        noStrings: true
      }, numericality)
    }, extend)
  }

  /** presence = true, onlyInteger = true, noStrings = true */
  public integerStrictMandatory(numericality?: NumericalityConstraint, extend?: ClientConstraints): ClientConstraints {
    return validate.extend({
      presence: true,
      numericality: validate.extend({
        onlyInteger: true,
        noStrings: true
      }, numericality)
    }, extend)
  }

  /** string = true */
  public string(extend?: ClientConstraints): ClientConstraints {
    return validate.extend({
      string: true
    }, extend)
  }

  /** presence = true, string = true */
  public stringMandatory(extend?: ClientConstraints): ClientConstraints {
    return validate.extend({
      presence: true,
      string: true
    }, extend)
  }

  /** presence  = true, boolean = true */
  public booleanMandatory(extend?: ClientConstraints): ClientConstraints {
    return validate.extend({
      presence: true,
      boolean: true
    }, extend)
  }

  /** boolean = true */
  public boolean(extend?: ClientConstraints): ClientConstraints {
    return validate.extend({
      boolean: true
    }, extend)
  }
}

export class ConstraintBuilder {
  /** array = (param) or true */
  public array<T>(arrayConstraint?: ConstraintOrFactory<ArrayConstraint> | Constraints, extend?: Constraints): Constraints {
    return validate.extend({
      array: arrayConstraint || true
    }, extend as any)
  }

  /** array = (param) or true, presence = true */
  public arrayMandatory<T>(arrayConstraint?: ConstraintOrFactory<ArrayConstraint> | Constraints, extend?: Constraints): Constraints {
    return validate.extend({
      array: arrayConstraint || true,
      presence: true
    }, extend as any)
  }

  /** array = true, validator (param) */
  public arrayValidator<T>(itemValidator: IValidatorSpec<T>, extend?: Constraints): Constraints {
    return validate.extend({
      array: true,
      validator: itemValidator
    }, extend)
  }

  /** presence = true, validator (param) */
  public arrayValidatorMandatory<T>(itemValidator: IValidatorSpec<T>, extend?: Constraints): Constraints {
    return validate.extend({
      array: true,
      presence: true,
      validator: itemValidator
    }, extend)
  }

  /** validator (param) */
  public itemValidator<T>(itemValidator: IValidatorSpec<T>, extend?: Constraints): Constraints {
    return validate.extend({
      validator: itemValidator
    }, extend)
  }

  /** array = true, presence = true, validator (param) */
  public itemValidatorMandatory<T>(itemValidator: IValidatorSpec<T>, extend?: Constraints): Constraints {
    return validate.extend({
      presence: true,
      validator: itemValidator
    }, extend)
  }

  /** onlyInteger = true, noStrings = true */
  public integerStrict(numericality?: NumericalityConstraint, extend?: Constraints): Constraints {
    return validate.extend({
      numericality: validate.extend({
        onlyInteger: true,
        noStrings: true
      }, numericality)
    }, extend)
  }

  /** presence = true, onlyInteger = true, noStrings = true */
  public integerStrictMandatory(numericality?: NumericalityConstraint, extend?: Constraints): Constraints {
    return validate.extend({
      presence: true,
      numericality: validate.extend({
        onlyInteger: true,
        noStrings: true
      }, numericality)
    }, extend)
  }

  /** string = true */
  public string(extend?: Constraints): Constraints {
    return validate.extend({
      string: true
    }, extend)
  }

  /** presence = true, string = true */
  public stringMandatory(extend?: Constraints): Constraints {
    return validate.extend({
      presence: true,
      string: true
    }, extend)
  }

  /** presence  = true, boolean = true */
  public booleanMandatory(extend?: Constraints): Constraints {
    return validate.extend({
      presence: true,
      boolean: true
    }, extend)
  }

  /** boolean = true */
  public boolean(extend?: Constraints): Constraints {
    return validate.extend({
      boolean: true
    }, extend)
  }

  public mustValidator<T>(validator: (value: T) => MustConstraintFunctionReturnType): MustConstraintFunction<T> {
    return async (value: T | T[]) => {
      if (validate.isEmpty(value)) {
        return true;
      }
      if (!validate.isArray(value)) {
        return validator(value);
      }
      for (let i = 0; i < value.length; i++) {
        let validated = validator(value[i])
        let result = isPromise(validated) ? await validated : validated
        if (result === false || validate.isString(result) || validate.isArray(result)) {
          return result;
        }
      }
    }
  }
}
