import { Validator, IValidatorSpec, validate, IValidator, ConstraintSpec } from "../../index";
import { MustConstraintFunction } from "../../validate.js";

export interface IInvalid {
  id: number
  isActive: boolean;
  roleIds?: number[]
  errorEarly?: string
}

function throwRuntimeError<T>(): MustConstraintFunction<string> {
  return () => new Promise((res, rej) => {
    rej(new Error("RuntimeError"))
  });
}

/** Recursive Validation example (friends) */
export class InvalidValidator extends Validator<IInvalid>{
  constraints(): ConstraintSpec<IInvalid> {
    return {
      id: {
        numericality: true,
        presence: true
      },
      isActive: this.booleanMandatory(),
      roleIds: {
        array: true,
        must: {
          func: this.mustValidator<number>(v => v < 100),
          message: "must be greater than zero"
        }
      },
      errorEarly: {
        must: {
          func: throwRuntimeError(),
          message: "Always error early"
        }
      }
    };
  }
}
