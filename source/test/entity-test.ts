import { expect, sinon, supertest, mocha } from "rokot-test";
import { IValidator, Validator, IValidatorSpec, validate, ConstraintSpec, IValidationError } from "../index";
import { IEntity, ISubEntity, EntityValidator } from "./example/entity";
import "chai-as-promised";
function validEntity(): IEntity {
  return { id: "1", members: [1, 2], subEntities: [{ id: "2" }] }
}

describe("Entity Validator", () => {
  it("should validate with valid Entity", () => {
    var entity = validEntity();
    return expect(new EntityValidator().validate(entity)).to.eventually.be.fulfilled.deep.equal(entity);
  });

  it("should fail with invalid Entity (explicit id absence)", () => {
    var entity = validEntity();
    return expect(new EntityValidator(false).validate(entity)).to.eventually.be.rejected.then(err => {
      const errors: IValidationError[] = err.errors;
      expect(errors).to.exist;
      expect(errors.length).to.be.eq(1);
      expect(errors[0].attribute).to.be.eq("id");
    });
  });
})
