import { expect, sinon, supertest } from "rokot-test";
import { createValidator, IValidator, Validator, IValidatorSpec, validate, ConstraintSpec, IValidationError } from "../index";

interface IChild {
  name: string
  values: string[]
  intvalues: number[]
}

interface IChildContainer {
  items: IChild[]
  manItems: IChild[]
}

const cv = createValidator<IChild>(b => {
  return {
    name: b.stringMandatory(),
    values: b.array(b.stringMandatory({length: {minimum:1}})),
    intvalues: b.array(b.integerStrictMandatory())
  }
})
const v = createValidator<IChildContainer>(b => {
  return {
    items: b.arrayValidator(cv),
    manItems: b.arrayValidatorMandatory(cv)
  }
})


describe("Array Validator", () => {
  it("should fail on empty name", () => {
    return expect(v.validate({manItems:[{name:"a"}], items: [{name: "", values:[] }]})).to.eventually.be.rejected.then(err => {
      const errors: IValidationError[] = err.errors;
      console.log(JSON.stringify(errors, null, 2))
      expect(errors).to.exist;
      expect(errors.length).to.be.eq(1);
      expect(errors[0].attribute).to.be.eq("items.0.name");
      // expect(errors[1].attribute).to.be.eq("isActive");
      // expect(errors[2].attribute).to.be.eq("roleIds");
      // expect(errors[3].attribute).to.be.eq("friends.1.id");
      // expect(errors[4].attribute).to.be.eq("friends.1.isActive");
    });
  });
    it("should fail on empty value", () => {
    return expect(v.validate({manItems:[{name:"a"}], items: [{name: "name", intvalues:["a"], values:["ok", "", ""] }]})).to.eventually.be.rejected.then(err => {
      const errors: IValidationError[] = err.errors;
      console.log(JSON.stringify(errors, null, 2))
      expect(errors).to.exist;
      expect(errors.length).to.be.eq(3);
      expect(errors[0].attribute).to.be.eq("items.0.values");
      expect(errors[1].attribute).to.be.eq("items.0.values");
      expect(errors[2].attribute).to.be.eq("items.0.intvalues");
      // expect(errors[0].attribute).to.be.eq("id");
      // expect(errors[1].attribute).to.be.eq("isActive");
      // expect(errors[2].attribute).to.be.eq("roleIds");
      // expect(errors[3].attribute).to.be.eq("friends.1.id");
      // expect(errors[4].attribute).to.be.eq("friends.1.isActive");
    });
  });
})
