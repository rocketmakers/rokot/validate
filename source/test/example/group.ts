import { Validator, IValidatorSpec, validate, IValidator, ConstraintSpec } from "../../index";
export interface IGroup {
  id: number
  name: string;
  members: number[]
  groupCategoryId?: number;
  active: boolean;
}

export class GroupValidator extends Validator<IGroup>{
  constraints(): ConstraintSpec<IGroup> {
    return {
      id: this.integerStrictMandatory(undefined, {must: {
        func: (value, attr) => {
          //console.log("GROUP", value, attr)
          return Promise.resolve(value > 0)
        },
        message: "does not exist"
      }}),
      name: this.stringMandatory(),
      groupCategoryId: this.integerStrict(undefined, {must: [
        {
          func: this.mustValidator<number>(v => {
            if (!validate.isNumber(v)) {
              return true;
            }
            //console.log("MV >", typeof v, v > 0)
            return v > 0
          }),
          message: "must be greater than zero"
        }, {
          func: this.mustValidator<number>(v => {
            if (!validate.isNumber(v)) {
              return true;
            }
            //console.log("MV <", typeof v, v < 100)
            return v < 100
          }),
          message: "must be less than 100"
        }
      ]}),
      active: this.booleanMandatory(),
      members: {
        presence: true,
        array: true,
        must: {
          func: this.mustValidator<number>(v => {
            return !validate.isNumber(v) ? "not a number" : v > 0
          }),
          message: "must be greater than zero"
        }
      }
    };
  }
}
