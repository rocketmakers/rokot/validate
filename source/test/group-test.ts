import { expect, sinon, supertest } from "rokot-test";
import { IValidator, Validator, IValidatorSpec, validate, ConstraintSpec, IValidationError } from "../index";
import { IGroup, GroupValidator } from "./example/group";
import "chai-as-promised";

function validGroup(): IGroup {
  return { id: 1, name: "rocketmakers", members: [2], groupCategoryId: 1, active: true }
}

function expectSingleMemberError(modifier: (group: IGroup) => void, attribute: string, count = 1) {
  var entity = validGroup();
  modifier(entity);
  return expect(new GroupValidator().validate(entity)).to.eventually.be.rejected.then(err => {
    const errors: IValidationError[] = err.errors;
    expect(errors).to.exist;
    //console.log(errors)
    expect(errors.length).to.be.eq(count);
    errors.forEach(error => {
      expect(error.attribute).to.be.eq(attribute);
    })
  });
}
function expectValid(modifier?: (group: IGroup) => void, compareModifier?: (group: IGroup) => IGroup) {
  var entity = validGroup();
  if (modifier) {
    modifier(entity);
  }
  return expect(new GroupValidator().validate(entity)).to.eventually.be.fulfilled.deep.equal(compareModifier ? compareModifier(entity) : entity);
}

describe("Group Validator", () => {
  it("should validate Group", () => {
    return expectValid();
  });

  it("should validate Group with extra props", () => {
    return expectValid(g => {
      g["extra"] = "extra"
    }, g => {
      return validGroup();
    });
  });

  it("should fail with invalid Group (bad member non numeric)", () => {
    return expectSingleMemberError(e => e.members.push("arghh" as any), "members");
  });

  it("should fail with invalid Group (bad member zero)", () => {
    return expectSingleMemberError(e => e.members.push(0), "members");
  });

  it("should fail with invalid Group (bad member negative)", () => {
    return expectSingleMemberError(e => e.members.push(-1), "members");
  });

  it("should fail with invalid Group (no members)", () => {
    return expectSingleMemberError(e => e.members.pop(), "members");
  });
  it("should fail with invalid Group (missing members)", () => {
    return expectSingleMemberError(e => delete e.members, "members");
  });

  it("should fail with invalid Group (name not string)", () => {
    return expectSingleMemberError(e => e.name = 100 as any, "name");
  });

  it("should fail with invalid Group (name missing)", () => {
    return expectSingleMemberError(e => delete e.name, "name");
  });

  it("should fail with invalid Group (groupCategoryId not number)", () => {
    return expectSingleMemberError(e => e.groupCategoryId = "1" as any, "groupCategoryId");
  });

  it("should fail with invalid Group (groupCategoryId zero)", () => {
    return expectSingleMemberError(e => e.groupCategoryId = 0, "groupCategoryId");
  });

  it("should fail with invalid Group (groupCategoryId negative)", () => {
    return expectSingleMemberError(e => e.groupCategoryId = -1, "groupCategoryId");
  });

  it("should fail with invalid Group (groupCategoryId too big)", () => {
    return expectSingleMemberError(e => e.groupCategoryId = 100 as any, "groupCategoryId");
  });

  it("should validate Group (optional groupCategoryId missing)", () => {
    return expectValid(e => delete e.groupCategoryId);
  });

  it("should fail with invalid Group (active not boolean)", () => {
    return expectSingleMemberError(e => e.active = "active" as any, "active");
  });

  it("should fail with invalid Group (active missing)", () => {
    return expectSingleMemberError(e => delete e.active, "active");
  });
})
