# rokot-validate

Rokot - [Rocketmakers](http://www.rocketmakers.com/) TypeScript NodeJs Platform

## Introduction

A typescript framework for Validation.

This library extends the `validate.js` npm with additional validators.

It provides abstract `Validator<T>` and `ValidatorWithContext<T, TContext>` class to create validator instances

The only difference between the 2 validators is that ValidatorWithContext provides a method to obtain context data that is needed within the creation of the `ConstraintSpec`

NOTE: When an object is validated it is returned 'clean' (e.g. attributes NOT specified on the validator will be removed)

## Getting Started

### Installation
Install via `npm`
```
npm i rokot-validate -S
```

## Example:

```typescript
import {ValidatorWithContext, Validator, ConstraintSpec} from "rokot-validate";

export interface IEntity {
  id: string;
  members?: number[];
  subEntities: ISubEntity[];
}
export interface ISubEntity {
  id: string
}

// Create the 'Validator Spec'
class SubEntityValidator extends Validator<ISubEntity> {
  constraints(): ConstraintSpec<ISubEntity> {
    return {
      id: this.mandatoryString()
    }
  }
}

export class EntityValidator extends ValidatorWithContext<IEntity, number>{
  getContext(entity, isRootValidator) {
    return Promise.resolve(1);
  }
  constraints(context: number) :ConstraintSpec<IEntity> {
    //console.log("context", context);
    return {
      id: this.mandatoryString(),
      deleteMe:{
        absence: true
      },
      subEntities: {
        array: true,
        presence: true,
        validator: new SubEntityValidator()
      },
      members: {
        array: {
          numericality: this.integerStrict({greaterThan:0})
        }
      }
    };
  }
}

const validator = new EntityValidator();
validator.validate({ id: "1",members:[1,2], subEntities:[{id:"2"}], extraneous:true })
  .then((entity: IEntity) => {
    // you now have a clean and verified entity
    // e.g. {id: "1",members:[1,2], subEntities:[{id:"2"}] }
  })
  .catch(error => {
    // the error might be a real error (if your custom functions failed)
    // or most likely it will be a ValidationError (implements IValidationErrors)
  })
```

## Additional Validators

### string
Ensure the value is a string

Example: name is a mandatory string
```typescript
name: {
  presence: true,
  string: true
}
```

### boolean
Ensure the value is a boolean

Example: active is a mandatory boolean
```typescript
active: {
  presence: true,
  boolean: true
}
```

### array
Ensure the value is an array

Example: tags is an array
```typescript
tags: {
  array: true
}
```

Example: counts is a numeric array
```typescript
counts: {
  array: {
    numericality: true
  }
}
```

### absence
Ensure the property explicitly doesn't exist on the object

```typescript
notAllowed: {
  absence: true
}
```


### defined
Ensure the value is not `null` or `undefined`

### must
Allows a custom function (or functions) to determine validity


Example: single function to check existence in database
```typescript
roleId: {
  must: {
    func: this.mustValidator<string>(id => db.checkRoleId(id)),
    message: "does not exist"
  }
}
```

Example: multiple functions to check existence in database, and perform secondary check
```typescript
roleId: {
  must: [{
    func: this.mustValidator<string>(id => db.checkRoleId(id)),
    message: "does not exist"
  },{
    func: this.mustValidator<string>(id => db.someOtherCheck(id)),
    message: "does not pass other check"
  }]
}
```

Example: single function to check existence in database (called for each item in array)
```typescript
roleIds: {
  array: true,
  must: {
    func: this.mustValidator<string>(id => db.checkRoleId(id)),
    message: "does not exist"
  }
}
```

### validator
Allows you to specify a child Validator for the property

Example: validate friend objects with a secondary validator
```typescript
friends:{
  array:true,
  validator: new FriendValidator()
}
```

## Consumed Libraries

### [validate.js](https://validatejs.org/)
Declarative validating of javascript objects

## Contributing

### Getting started

Install `node_modules` via `npm`
```
npm i
```

Install `typings`
```
typings install
```

Build the project (using typescript compiler)
```
npm run build
```

Test the project (builds before testing)
```
npm test
```
