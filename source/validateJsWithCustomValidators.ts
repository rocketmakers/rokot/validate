import * as moment from "moment";
import { ValidateJsValidator, MustConstraint, MustConstraintFunctionReturnType, IValidatorSpec, ArrayConstraint } from "./validate.js";
import { IValidationErrors } from "./errors";
const validate: ValidateJsValidator = require("validate.js");
import "validate.js";
validate.Promise = Promise;

(validate as any).error = () => { }; // suppress silly error from validate.js

validate.validators.string = (value, options, key, attributes) => {
  if (validate.isDefined(value) && !validate.isString(value)) {
    return "is not a string";
  }
};

validate.validators.boolean = (value, options, key, attributes) => {
  if (validate.isDefined(value) && !(value === true || value === false)) {
    return "is not a boolean";
  }
};

validate.validators.array = (value, options: ArrayConstraint, key, attributes) => {
  if (!validate.isDefined(value)) {
    return;
  }

  if (!validate.isArray(value)) {
    return ["is not an array"];
  }

  if (options === true){
    return
  }

  const errors = (value as any[]).map((v, idx) => {
    const field = `'item ${idx}'`
    const vr = validate({[field]: v}, {[field]: options})
    if (vr){
      // const vrr = vr[idx]
      // if (validate.isArray(vrr)){
      //   vr[idx] = vrr.map(r => `'item ${idx}' ${r}:`)
      // }
    }
    return vr
  }).filter(r => !!r);
  if (errors.length) {
    //console.log("ERRORS: ", JSON.stringify(errors, null, 2))
    return errors.map(e => Object.keys(e).map(k => e[k].join(",")).join(":"))
  };

  // if (options.string) {
    
  //   const errors = (value as any[]).map(v => validate.validators.string(v, options)).filter(r => !!r);
  //   if (errors.length) {
  //     return errors.map(e => `array member ${e}`)
  //   };
  // }

  // if (options.numericality) {
  //   const errors = (value as any[]).map(v => validate.validators.numericality(v, options.numericality)).filter(r => !!r);
  //   if (errors.length) {
  //     return errors.map(e => `array member ${e}`)
  //   };
  // }
};

validate.validators.arrayNumericality = (value, options, key, attributes) => {
  if (!validate.isDefined(value) || !validate.isArray(value)) {
    return;
  }
};

validate.validators.absence = (value, options, key, attributes) => {
  if (!validate.single(value, { presence: true })) {
    return "must be blank";
  }
}

validate.validators.defined = (value, options, key, attributes) => {
  if (value === null) {
    return "is null";
  }

  if (value === undefined) {
    return "is undefined";
  }
}

/** This is a noop - simple spec validation - spec collected at runtime */
validate.validators.validator = (value, spec: IValidatorSpec<any>, key, attributes) => {
  if (!spec || !spec.constraints || !validate.isFunction(spec.constraints)) {
    throw new Error("'validator' validator should be set to a Validator instance");
  }
}

validate.validators.must = async (value, validator: MustConstraint<any> | MustConstraint<any>[], key, attributes) => {
  if (!validator) {
    throw new Error("validator should be provided for 'must' validator");
  }

  if (validate.isArray(validator)) {
    for (let i = 0; i < validator.length; i++) {
      let result = await tryValidate(validator[i], value, attributes)
      if (result) {
        return result;
      }
    }
  } else {
    return await tryValidate(validator, value, attributes);
  }
}

async function tryValidate(validator: MustConstraint<any>, value, attributes) {
  const aa = validator.func(value, attributes);
  const valid = isPromise(aa) ? await aa : aa
  if (validate.isString(valid) || validate.isArray(valid)) {
    return valid;
  }
  if (valid === false) {
    return validator.message ? validate.format(validator.message, attributes) : "condition not met";
  }
}

export function isPromise<T>(parameter: T | Promise<T>): parameter is Promise<T> {
  return !!(parameter as Promise<T>).then
}

validate.extend(validate.validators.datetime, {
  parse: (value: string, options) => {
    const utcMoment = moment.utc(value, moment.ISO_8601 as any, true);
    if (!utcMoment.isValid()) {
      return NaN;
    }

    return +utcMoment;
  },
  format: (value: number, options) => {
    var format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
    return moment.utc(value).format(format);
  }
});

export { validate };
