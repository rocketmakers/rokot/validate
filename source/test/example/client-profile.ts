import { ClientConstraintSpec, createClientConstraintSpec, ClientConstraints } from "../../index";

export interface IClientProfile {
  id: number
  isActive: boolean;
  friends?: IClientProfile[]
  bestFriend?: IClientProfile
  roleIds?: number[]
}

/** Recursive Validation example (friends) */
export const clientProfileSpec = createClientConstraintSpec<IClientProfile>(b => ({
      id: {
        numericality: true,
        presence: true
      },
      bestFriend: b.itemValidator<IClientProfile>({id:{},isActive:{},friends:{}, bestFriend:{}, roleIds:{}}),
      isActive: b.booleanMandatory(),
      friends: {
        array: true
      },
      roleIds: {
        array: true
      }
    }));

clientProfileSpec.friends.spec = clientProfileSpec
