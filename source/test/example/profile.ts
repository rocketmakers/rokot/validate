import { Validator, IValidatorSpec, validate, IValidator, ConstraintSpec } from "../../index";
export interface IProfile {
  id: number
  isActive: boolean;
  friends?: IProfile[]
  roleIds?: number[]
}

/** Recursive Validation example (friends) */
export class ProfileValidator extends Validator<IProfile>{
  constraints(): ConstraintSpec<IProfile> {
    return {
      id: {
        numericality: true,
        presence: true
      },
      isActive: this.booleanMandatory(),
      friends: {
        array: true,
        validator: new ProfileValidator()
      },
      roleIds: {
        array: true,
        must: {
          func: this.mustValidator<number>(v => v > 0),
          message: "must be greater than zero"
        }
      }
    };
  }
}
