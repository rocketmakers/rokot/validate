import { validate } from "./validateJsWithCustomValidators";
import { IValidator, IValidatorsSpec, IValidatorSpecWithContext, IValidatorSpec, ConstraintSpec, ClientConstraintSpec } from "./validate.js";
import { Errors, IValidationError } from "./errors";

interface IValidationContext {
  constraints: ConstraintSpec<any>;
  validators: IValidatorsSpec
  instance: Object;
  key: string;
  parentKey: string;
}

class VisitData<TStored>{
  private id = 1;
  private visited: any[] = [];
  private stored: TStored[] = [];
  getNextId() {
    return this.id++;
  }

  store(visit: any, store: TStored) {
    this.visited.push(visit);
    this.stored.push(store);
  }

  findStored(visit: any) {
    const index = this.visited.indexOf(visit);
    if (index > -1) {
      return this.stored[index];
    }
  }
}

interface IValidationResult {
  id: number;
  ref?: number;
  errors: IValidationError[];
  context: IValidationContext;
  children?: IValidationResult[];
}

class Cleaner {
  static recursivelyClean<T>(validationResult: IValidationResult, visitData: VisitData<any>): T {
    const instance = validationResult.context.instance;
    if (!validate.isDefined(instance)) {
      return instance as T;
    }

    const stored = visitData.findStored(instance);
    if (stored) {
      return stored as T;
    }

    const cleaned = validate.cleanAttributes<T>(instance, validationResult.context.constraints);
    visitData.store(instance, cleaned);
    if (validationResult.children) {
      validationResult.children.forEach(c => {
        const clean = this.recursivelyClean(c, visitData);
        const parts = c.context.key.split(".");
        let parent = cleaned;
        const lastIndex = parts.length - 1;
        for (let i = 0; i < lastIndex; i++) {
          parent = parent[parts[i]];
        }
        parent[parts[lastIndex]] = clean;
      })
    }
    return cleaned;
  }
}

class ValidationResultHelper {
  static makeRef(key: string, parentKey: string, ref: number): IValidationResult {
    return { errors: [], context: { key, parentKey }, ref } as any;
  }
  static resolveRefs(result: IValidationResult) {
    const refs: IValidationResult[] = []
    const resultById = this.collectResultById(result, {}, refs)
    refs.forEach(r => {
      r.context = validate.extend<IValidationContext>({} as any, resultById[r.ref].context, r.context);
    })
  }

  private static collectResultById(result: IValidationResult, results: { [key: number]: IValidationResult }, refs: IValidationResult[]) {
    if (result.ref) {
      refs.push(result)
      return results;
    }
    if (result.id) {
      results[result.id] = result;
    }
    if (result.children) {
      result.children.forEach(c => {
        this.collectResultById(c, results, refs);
      })
    }
    return results;
  }

  static collectErrors(result: IValidationResult, collect: IValidationError[]) {
    if (result.errors && result.errors.length) {
      collect.push(...result.errors);
    }
    if (result.children) {
      result.children.forEach(c => {
        if (!c) {
          return;
        }
        this.collectErrors(c, collect);
      })
    }
    return collect;
  }
}


function createClientValidatorSpec<T>(constraintSpec: ClientConstraintSpec<T>): IValidatorSpec<T> {
  return {
    constraints(): ConstraintSpec<T> {
      return constraintSpec
    }
  }
}

export class Validation {
  public static executeClient<T>(instance: any, spec: ClientConstraintSpec<T>): Promise<T> {
    return Validation.execute(instance, createClientValidatorSpec(spec))
  }

  public static async execute<T>(instance: any, validation: IValidatorSpec<T>) {
    if (!instance) {
      throw new Errors.NoValidationInputError();
    }
    const result = await this.recursivelyValidate(new VisitData<number>(), instance, validation)
    const errors = ValidationResultHelper.collectErrors(result, [])
    if (errors.length) {
      throw new Errors.ValidationError(errors);
    }
    ValidationResultHelper.resolveRefs(result);
    return Cleaner.recursivelyClean<T>(result, new VisitData<any>());
  }

  private static async getValidationContext<T>(spec: IValidatorSpec<T>, instance: Object, key: string, parentKey: string): Promise<IValidationContext> {
    if (this.hasContext(spec)) {
      const ctx = await spec.getContext(instance, !key)
      return this.makeValidationContext(spec, instance, key, parentKey, ctx);
    }

    return this.makeValidationContext(spec, instance, key, parentKey);
  }

  private static makeValidationContext<T>(spec: IValidatorSpec<T>, instance: Object, key: string, parentKey: string, ctx?: any): IValidationContext {
    const constraints = spec.constraints(ctx);
    return {
      key,
      parentKey,
      constraints,
      instance,
      validators: this.gatherValidators(constraints)
    }
  }

  private static gatherValidators<T>(constraints: ConstraintSpec<T>) {
    const spec: IValidatorsSpec = {}
    let found = false;
    Object.keys(constraints).forEach((k: keyof T) => {
      const con = constraints[k];
      if (con.validator) {
        spec[k] = con.validator;
        found = true;
      } else if (con.spec) {
        spec[k] = createClientValidatorSpec(con.spec);
        found = true;
      }
    })
    return found ? spec : undefined
  }

  private static hasContext(spec: IValidatorSpec<any>): spec is IValidatorSpecWithContext<any, any> {
    return !!(spec as IValidatorSpecWithContext<any, any>).getContext;
  }

  private static async recursivelyValidate(visitData: VisitData<number>, instance: Object, spec: IValidatorSpec<any>, key?: string, parentKey?: string): Promise<IValidationResult> {
    let id: number = visitData.getNextId();
    if (validate.isDefined(instance)) {
      const stored = visitData.findStored(instance);
      if (stored) {
        return ValidationResultHelper.makeRef(key, parentKey, stored);
      }

      visitData.store(instance, id);
    }

    const context = await this.getValidationContext<any>(spec, instance, key, parentKey)
    return await this.validateContext(visitData, id, context)
  }

  private static async validateContext(visitData: VisitData<number>, id: number, validationContext: IValidationContext): Promise<IValidationResult> {
    const errors = await this.getValidationErrors(validationContext)
    const validationResult: IValidationResult = { id, errors: errors || [], context: validationContext }
    if (!validationContext.validators || !validationContext.instance) {
      return validationResult;
    }
    const results = await Validation.getChildValidationResults(visitData, validationContext)
    validationResult.children = results;
    return validationResult;
  }

  private static async getChildValidationResults(visitData: VisitData<number>, validationContext: IValidationContext): Promise<IValidationResult[]> {
    const results: IValidationResult[] = [];
    const keys = Object.keys(validationContext.validators)
    for (let i = 0; i < keys.length; i++) {
      let key = keys[i];
      let value = validationContext.instance[key];
      if (!validate.isDefined(value)) {
        continue;
      }

      let spec = validationContext.validators[key];
      if (validate.isArray(value)) {
        //console.log("Array", JSON.stringify(value))
        for (let idx = 0; idx < value.length; idx++) {
          let arrayKey = `${key}.${idx}`;
          let ve = await this.recursivelyValidate(visitData, value[idx], spec, arrayKey, this.makeKey(arrayKey, validationContext.parentKey))
          if (ve) {
            results.push(ve)
          }
        };
        continue;
      }
      let ve = await this.recursivelyValidate(visitData, value, spec, key, this.makeKey(key, validationContext.parentKey))
      if (ve) {
        results.push(ve)
      }
    }
    return results;
  }

  private static makeKey(key: string, parentKey: string) {
    return parentKey ? `${parentKey}.${key}` : key;
  }

  private static async getValidationErrors(validationContext: IValidationContext): Promise<IValidationError[]> {
    try {
      return await validate.async(validationContext.instance, validationContext.constraints, { format: "detailed" })
    } catch (errors) {
      if (!validate.isArray(errors)) {
        // it is a real error!
        throw errors;
      }

      return errors.map(d => {
        return <IValidationError>{
          message: d.error,
          value: d.value,
          attribute: validationContext.parentKey ? `${validationContext.parentKey}.${d.attribute}` : d.attribute
        };
      });
    }
  }
}
