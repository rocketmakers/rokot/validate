import { validate, isPromise } from "./validateJsWithCustomValidators";
import { IValidator, IValidatorSpec, IValidatorSpecWithContext, NumericalityConstraint, ConstraintSpec, ClientConstraintSpec } from "./validate.js";
import { Validation } from "./validation";
import { ConstraintBuilder, ClientConstraintBuilder } from "./constraintBuilder";

export abstract class ValidatorBase<T> extends ConstraintBuilder implements IValidator<T>, IValidatorSpec<T> {
  public validate(instance: any): Promise<T> {
    return Validation.execute<T>(instance, this)
  }

  abstract constraints(context?: any): ConstraintSpec<T>;
}

export abstract class Validator<T> extends ValidatorBase<T>{
  abstract constraints(): ConstraintSpec<T>;
}

export function createValidator<T>(constraintSpecBuilder: (builder: ConstraintBuilder) => ConstraintSpec<T>): Validator<T> {
  class V extends Validator<T>{
    constraints(): ConstraintSpec<T> {
      return constraintSpecBuilder(this)
    }
  }

  return new V()
}

const clientConstraintBuilder = new ClientConstraintBuilder()
export function createClientConstraintSpec<T>(constraintSpecBuilder: (builder: ClientConstraintBuilder) => ClientConstraintSpec<T>): ClientConstraintSpec<T> {
  return constraintSpecBuilder(clientConstraintBuilder)
}

export abstract class ValidatorWithContext<T, TContext> extends ValidatorBase<T> implements IValidatorSpecWithContext<T, TContext> {
  abstract getContext(instance: any, isRootValidator?: boolean): Promise<TContext>;
  abstract constraints(context: TContext): ConstraintSpec<T>;
}
