import { ValidatorWithContext, Validator, IValidatorSpec, ConstraintSpec, validate } from "../../index";

export interface IEntity {
  id: string;
  members?: number[];
  subEntities: ISubEntity[];
}

export interface ISubEntity {
  id: string
}

// Create the 'Validator Spec'
class SubEntityValidator extends Validator<ISubEntity> {
  constraints(): ConstraintSpec<ISubEntity> {
    return {
      id: this.stringMandatory()
    }
  }
}

export class EntityValidator extends ValidatorWithContext<IEntity, number>{
  constructor(private allowId = true){
    super()
  }
  getContext(entity, isRootValidator) {
    return Promise.resolve(1);
  }
  constraints(context: number): ConstraintSpec<IEntity> {
    return {
      id: this.allowId ? this.stringMandatory() : {absence: true},
      subEntities: this.arrayValidatorMandatory(new SubEntityValidator()),
      members: this.array(this.integerStrict({ greaterThan: 0 }))
    };
  }
}
